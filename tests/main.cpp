#include <wheels/test/test_framework.hpp>

#include <carry/context.hpp>
#include <carry/empty.hpp>
#include <carry/wrap.hpp>
#include <carry/new.hpp>
#include <carry/mutable.hpp>
#include <carry/current.hpp>
#include <carry/scope.hpp>

#include <carry/carrier.hpp>

#include <compass/map.hpp>

#include <optional>

TEST_SUITE(Context) {
  SIMPLE_TEST(Entries) {
    auto ctx = carry::New()
        .SetInt64("test", 123)
        .SetString("id", "12345")
        .Done();

    ASSERT_EQ(ctx.Get<int64_t>("test"), 123);
    ASSERT_EQ(ctx.Get<std::string>("id"), "12345");

    ASSERT_FALSE(ctx.TryGet<int64_t>("missing"));

    ASSERT_EQ(ctx.GetOr<std::string>("missing", "or"), "or");
  }

  SIMPLE_TEST(DefaultCtor) {
    carry::Context ctx;
    ASSERT_FALSE(ctx.TryGet<std::string>("key"));
  }

  SIMPLE_TEST(Copy) {
    auto ctx1 = carry::New().SetInt64("test", 123).Done();

    auto ctx2 = ctx1;
    ASSERT_EQ(ctx2.Get<int64_t>("test"), 123);
  }

  SIMPLE_TEST(Wrap) {
    auto ctx1 = carry::New()
        .SetInt64("test", 123)
        .Done();

    auto ctx2 = carry::Wrap(ctx1)
        .SetInt64("test", 456)
        .SetInt64("y", 7)
        .Done();

    ASSERT_EQ(ctx1.Get<int64_t>("test"), 123);
    ASSERT_EQ(ctx2.Get<int64_t>("test"), 456);
    ASSERT_EQ(ctx2.Get<int64_t>("y"), 7);
    ASSERT_FALSE(ctx1.TryGet<int64_t>("y"));
  }

  SIMPLE_TEST(WrapImplicitDone) {
    auto ctx1 = carry::Empty();

    carry::Context ctx2 = carry::Wrap(ctx1).SetString("key", "test");

    ASSERT_EQ(ctx2.GetString("key"), "test");
  }

  SIMPLE_TEST(Wrapper) {
    auto ctx1 = carry::New()
        .SetInt64("test", 123)
        .Done();

    carry::Wrapper wrapper(ctx1);
    wrapper.SetInt64("test", 456);
    wrapper.SetInt64("port", 80).SetString("name", "test");

    auto ctx2 = wrapper.Done();

    ASSERT_EQ(ctx2.GetInt64("test"), 456);
    ASSERT_EQ(ctx2.GetInt64("port"), 80);
    ASSERT_EQ(ctx2.GetString("name"), "test")
  }

  SIMPLE_TEST(Builder) {
    carry::Builder builder;

    builder.SetString("key", "value");
    builder.SetInt64("port", 80).SetBool("flag", true);

    auto ctx = builder.Done();

    ASSERT_EQ(ctx.GetString("key"), "value");
    ASSERT_EQ(ctx.GetInt64("port"), 80);
    ASSERT_EQ(ctx.GetBool("flag"), true);
  }

  SIMPLE_TEST(GetPanics) {
    auto ctx = carry::New().SetInt64("key", 123).Done();
    // ctx.GetUInt64("key");  // Panics
  }

  SIMPLE_TEST(CollectKeys) {
    auto ctx1 = carry::New()
        .SetString("header.timeout", "100500")
        .SetString("header.trace_id", "abc-123")
        .SetString("key1", "value1")
        .Done();

    auto ctx2 = carry::Wrap(ctx1)
        .SetString("header.timeout", "1000")
        .SetString("key2", "value2")
        .Done();

    auto headers = ctx2.CollectKeys("header.");

    ASSERT_EQ(headers.size(), 2);

    std::set<std::string> expected_headers;
    expected_headers.insert("header.timeout");
    expected_headers.insert("header.trace_id");

    ASSERT_EQ(headers, expected_headers);
  }

  SIMPLE_TEST(Mutable) {
    carry::MutableContext mut_ctx;
    auto ctx = mut_ctx.View();

    mut_ctx.Set<std::string>("key1", "value1");
    mut_ctx.Set<int64_t>("key2", 100500);

    {
      ASSERT_EQ(ctx.Get<std::string>("key1"), "value1");
      ASSERT_EQ(ctx.Get<int64_t>("key2"), 100500);

      ASSERT_FALSE(ctx.TryGet<bool>("key3"));
    }

    mut_ctx.Set<bool>("key3", true);

    ASSERT_EQ(ctx.GetBool("key3"), true);
  }

  class Carrier :
      public carry::ICarrier,
      public compass::Locator<carry::ICarrier> {
   public:
    Carrier() : context_(carry::Empty()) {
      //
    }

    // carry::ICarrier

    void Set(carry::Context context) override {
      context_ = std::move(context);
    }

    const carry::Context& GetContext() override {
      return context_;
    }

    // compass::Locator

    carry::ICarrier* Locate(compass::Tag<carry::ICarrier>) override {
      return this;
    }

   private:
    carry::Context context_;
  };

  SIMPLE_TEST(Carrier) {
    Carrier carrier;

    compass::Map().Add(carrier);

    carrier.Set(carry::New().SetInt64("key", 7));

    {

      {
        auto value1 = carry::Current().TryGet<int64_t>("key");

        ASSERT_TRUE(value1);
        ASSERT_EQ(*value1, 7);
      }

      {
        carry::WrapScope<int64_t> scope("key", 9);

        auto value2 = carry::Current().TryGet<int64_t>("key");

        ASSERT_TRUE(value2);
        ASSERT_EQ(*value2, 9);
      }

      {
        auto value1 = carry::Current().TryGet<int64_t>("key");

        ASSERT_TRUE(value1);
        ASSERT_EQ(*value1, 7);
      }
    }

    compass::Map().Clear();
  }
}

RUN_ALL_TESTS()
